import telegram, logging

from modules.pytg.ModulesLoader import ModulesLoader

def accept_callback_handler(update, context):
    bot = context.bot

    query = update.callback_query
    query_data = query.data.split(",")
    user_id = query.from_user.id

    username = query.message.chat.username
    chat_id = query.message.chat_id
    message_id = query.message.message_id

    request_type = query_data[2]
    request_id = query_data[3]

    logging.info("Accepting request {} ({}, {})".format(request_id, request_type, chat_id))

    # Load request data
    data_manager = ModulesLoader.load_manager("data")
    request_data = data_manager.load_data("requests", request_id)

    # Forward request data to accept digester
    requests_manager = ModulesLoader.load_manager("requests")

    requests_manager.digesters[request_type]["accept"](bot, chat_id, message_id, request_data)

    # Remove buttons
    bot.editMessageReplyMarkup(
        chat_id = chat_id,
        message_id = message_id,
        reply_markup = None
    )

    # Delete request data
    data_manager.delete_data("requests", request_id)