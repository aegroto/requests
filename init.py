import logging

from modules.pytg.ModulesLoader import ModulesLoader

from modules.requests.RequestsManager import RequestsManager

from telegram.ext import CallbackQueryHandler

from modules.requests.handlers.callback.accept import accept_callback_handler
from modules.requests.handlers.callback.reject import reject_callback_handler

def load_callback_handlers(dispatcher):
    dispatcher.add_handler(CallbackQueryHandler(accept_callback_handler, pattern="requests,accept,.*"))
    dispatcher.add_handler(CallbackQueryHandler(reject_callback_handler, pattern="requests,reject,.*"))

def initialize():
    logging.info("Initializing requests module...")

    RequestsManager.initialize()

    bot_manager = ModulesLoader.load_manager("bot")

    load_callback_handlers(bot_manager.updater.dispatcher)

def connect():
    logging.info("Connecting requests module...")

def load_manager():
    return RequestsManager.load()

def depends_on():
    return ["bot", "data", "cache", "menu", "text"]