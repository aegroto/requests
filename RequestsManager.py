import telegram, yaml, logging

from modules.pytg.Manager import Manager
from modules.pytg.ModulesLoader import ModulesLoader

class RequestsManager(Manager):
    @staticmethod
    def initialize():
        RequestsManager.__instance = RequestsManager()

        return

    @staticmethod
    def load():
        return RequestsManager.__instance

    def __init__(self):
        self.digesters = { }

    ######################
    # Requests interface #
    ######################

    def add_digester(self, request_type, accept_func, reject_func):
        self.digesters[request_type] = {}

        self.digesters[request_type]["accept"] = accept_func
        self.digesters[request_type]["reject"] = reject_func

    def create_request(self, bot, user_chat_id, request_type, request_data, request_text_template):
        # Load managers
        data_manager = ModulesLoader.load_manager("data")
        cache_manager = ModulesLoader.load_manager("cache")
        config_manager = ModulesLoader.load_manager("config")
        menu_manager = ModulesLoader.load_manager("menu")

        # Generate an ID for the request
        cache = cache_manager.load_cache()
        request_id = cache["last_request_id"] + 1
        cache["last_request_id"] += 1
        cache_manager.save_cache(cache)

        # Add request specific field to data
        request_data["id"] = request_id
        request_data["type"] = request_type
        request_data["user_chat_id"] = user_chat_id

        # Create request data
        data_manager.create_data("requests", request_id)
        data_manager.save_data("requests", request_id, request_data)

        # Create request message
        requests_config = config_manager.load_settings_file("requests")

        # Replace macros
        for key in request_data.keys():
            text_key = "[{}]".format(key)

            request_text_template = request_text_template.replace(text_key, str(request_data[key]))

        # Send message
        bot.sendMessage(
            chat_id = requests_config["chat_ids"][request_type],
            text = request_text_template,
            reply_markup = menu_manager.create_reply_markup("request", meta = {
                "REQUEST_TYPE": str(request_type),
                "REQUEST_ID": str(request_id)
            }),
            parse_mode = telegram.ParseMode.MARKDOWN
        )